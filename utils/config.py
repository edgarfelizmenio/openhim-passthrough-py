import os
import json

def load_json_file(config_file):
    with open(os.path.join('config',config_file)) as config:
        data = json.load(config)
    return data