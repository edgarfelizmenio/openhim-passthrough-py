import datetime
import json
import requests

from flask import jsonify
from flask import request

from hello import app, urn

@app.route('/')
def index():
    return 'Index Page'

@app.route('/hello')
def hello_world():
    return 'Hello, World!'

@app.route('/encounters-py/<int:id>')
def get_encounter(id):
    try:
        response = requests.get('http://shr-tutorial.cs300ohie.net/encounters/{}'.format(id))
        contextObject = {}
        contextObject["encounter"] = response.json()

        orchestrationResults = []
        orchestrationResults.append({
            'name': 'Get Encounter',
            'request': {
                'path': request.path,
                'headers': {k:v for k,v in request.headers.to_list()},
                'querystring': request.query_string.decode('utf-8'),
                'body': request.data.decode('utf-8'),
                'method': request.method,
                'timestamp': str(int(datetime.datetime.now().timestamp()*100))
            },
            'response': {
                'status': response.status_code,
                'body': json.dumps(response.json()),
                'timestamp': str(int(datetime.datetime.now().timestamp()*100))
            }
        })

        orchestrations = [{
            'ctxObjectRef': "client",
            'name': "Get Client", 
            'domain': "http://cr-tutorial.cs300ohie.net",
            'path': "/patient/{}".format(response.json()["patientId"]),
            'params': "",
            'body': "",
            'method': "GET",
            'headers': ""
        }, {
            'ctxObjectRef': "provider",
            'name': "Get Provider", 
            'domain': "http://hwr-tutorial.cs300ohie.net",
            'path': "/providers/{}".format(response.json()["providerId"]),
            'params': "",
            'body': "",
            'method': "GET",
            'headers': ""
        }]

        for orchestration in orchestrations:
            orchestration_url = orchestration["domain"] + orchestration["path"] + orchestration["params"]
            orchestration_response = requests.get(orchestration_url)
            orchestrationResults.append({
                'name': orchestration["name"],
                'request': {
                    'path': orchestration["path"],
                    'headers': orchestration["headers"],
                    'querystring': orchestration["params"],
                    'body': orchestration["body"],
                    'method': orchestration["method"],
                    'timestamp': str(int(datetime.datetime.now().timestamp()*100))
                },
                'response': {
                    'status': orchestration_response.status_code,
                    'body': json.dumps(orchestration_response.json()),
                    'timestamp': str(int(datetime.datetime.now().timestamp()*100))
                }
            })
            contextObject[orchestration["ctxObjectRef"]] = orchestration_response.json()

        observationsHtml = ''
        for i in range(len(contextObject["encounter"]["observations"])):
            observationsHtml += '    <tr>' + "\n"
            '      <td>' + contextObject["encounter"]["observations"][i]["obsType"] + '</td>' + "\n"
            '      <td>' + contextObject["encounter"]["observations"][i]["obsType"] + '</td>' + "\n"
            '      <td>' + contextObject["encounter"]["observations"][i]["obsType"] + '</td>' + "\n"
            '    </tr>' + "\n"

        healthRecordHtml = '  <h3>Patient ID: #' + str(contextObject["encounter"]["patientId"]) + '</h3>' + "\n"
        '  <h3>Provider ID: #' + str(contextObject["encounter"]["providerId"]) + '</h3>' + "\n"
        '  <h3>Encounter Type: ' + contextObject["encounter"]["encounterType"] + '</h3>' + "\n"
        '  <h3>Encounter Date: ' + contextObject["encounter"]["encounterDate"] + '</h3>' + "\n"
        '  <table cellpadding="10" border="1" style="border: 1px solid #000; border-collapse: collapse">' + "\n"
        '    <tr>' + "\n"
        '      <td>Type:</td>' + "\n"
        '      <td>Value:</td>' + "\n"
        '      <td>Unit:</td>' + "\n"
        '    </tr>' + "\n"
        observationsHtml 
        '  </table>' + "\n"

        patientRecordHtml = ('  <h2>Patient Record: #' + str(contextObject["client"]["patientId"]) + '</h2>' + "\n" +
        '  <table cellpadding="10" border="1" style="border: 1px solid #000; border-collapse: collapse">' + "\n" +
        '    <tr>' + "\n" +
        '      <td>Given Name:</td>' + "\n" +
        '      <td>' + contextObject["client"]["givenName"] + '</td>' + "\n" +
        '    </tr>' + "\n" +
        '    <tr>' + "\n" +
        '      <td>Family Name:</td>' + "\n" +
        '      <td>' + contextObject["client"]["familyName"] + '</td>' + "\n" +
        '    </tr>' + "\n" +
        '    <tr>' + "\n" +
        '      <td>Gender:</td>' + "\n" +
        '      <td>' + contextObject["client"]["gender"] + '</td>' + "\n" +
        '    </tr>' + "\n" +
        '    <tr>' + "\n" +
        '      <td>Phone Number:</td>' + "\n" +
        '      <td>' + contextObject["client"]["phoneNumber"] + '</td>' + "\n" +
        '    </tr>' + "\n" +
        '  </table>' + "\n")

        providerRecordHtml = ('  <h2>Provider Record: #' + str(contextObject['provider']["providerId"]) + '</h2>' + "\n" +
        '  <table cellpadding="10" border="1" style="border: 1px solid #000; border-collapse: collapse">' + "\n" +
        '    <tr>' + "\n" +
        '      <td>Title:</td>' + "\n" +
        '      <td>' + contextObject['provider']["title"] + '</td>' + "\n" +
        '    </tr>' + "\n" +
        '    <tr>' + "\n" +
        '      <td>Given Name:</td>' + "\n" +
        '      <td>' + contextObject['provider']["givenName"] + '</td>' + "\n" +
        '    </tr>' + "\n" +
        '    <tr>' + "\n" +
        '      <td>Family Name:</td>' + "\n" +
        '      <td>' + contextObject['provider']["familyName"] + '</td>' + "\n" +
        '    </tr>' + "\n" +
        '  </table>' + "\n")

        responseBodyHtml = ('<html>' + "\n" +
        '<body>' + "\n" +
        '  <h1>Health Record</h1>' + "\n" +
        healthRecordHtml +
        patientRecordHtml +
        providerRecordHtml + 
        '</body>' + "\n" +
        '</html>')

        status = 'Successful'
        response = {
            'status': response.status_code,
            'headers': {
                'content-type': 'text/html'
            },
            'body': responseBodyHtml,
            'timestamp': str(int(datetime.datetime.now().timestamp()*100))
        }

        properties = {}
        for o in contextObject["encounter"]["observations"]:
            properties[o["obsType"]] = o["obsValue"] + o["obsUnit"]
        properties[contextObject["client"]["givenName"] + " " + contextObject["client"]["familyName"] + '(' + contextObject["client"]["gender"] + ')'] = contextObject["client"]["phoneNumber"]
        properties[contextObject["provider"]["title"]] = contextObject["provider"]["givenName"] + ' ' + contextObject["provider"]["familyName"]

        app.logger.info("orchestrations results: %s", orchestrationResults)

        returnObject = {
            "x-mediator-urn": urn,
            "status": status,
            "response": response,
            "orchestrations": orchestrationResults,
            "properties": properties
        }
        mediatorResponse = jsonify(returnObject)
        mediatorResponse.headers["Content-Type"] = 'application/json+openhim'

        return mediatorResponse

    except Exception as e:
        app.logger.error(e)
        exit()