import threading
import time
import logging

from flask import Flask

from utils.auth import *
from utils.config import load_json_file
from utils.mediator import *

logging.basicConfig(level=logging.INFO)

app = Flask(__name__)
appLogger = logging.StreamHandler()
appLogger.setFormatter(logging.Formatter(logging.BASIC_FORMAT))
app.logger.addHandler(appLogger)
app.logger.setLevel(logging.INFO)

config = {}
apiConfig = load_json_file("config.json")
mediatorConfig = load_json_file("mediator.json")
username = apiConfig["api"]["username"]
password = apiConfig["api"]["password"]
apiUrl = apiConfig["api"]["apiURL"]
rejectUnauthorized = not bool(apiConfig["api"]["trustSelfSigned"])
urn = mediatorConfig["urn"]
port = int(mediatorConfig["endpoints"][0]["port"])
heartbeat_interval = 10

logging.info('apiConfig: %s', apiConfig)
logging.info('mediatorConfig: %s', mediatorConfig)
logging.info('username: %s', username)
logging.info('password: %s', password)
logging.info('api url: %s', apiUrl)
logging.info('rejectUnauthorized: %s', rejectUnauthorized)
logging.info('urn: %s', urn)
logging.info('port: %s', port)
logging.info('heartbeat interval: %s', heartbeat_interval)

try:
    # register mediator to OpenHIM
    register_mediator(username, password, apiUrl, rejectUnauthorized, mediatorConfig)

    # fetch config from OpenHIM
    new_config = fetch_config(username, password, apiUrl, rejectUnauthorized, urn)
    if (len(new_config) > 0):
        config = new_config

    # install mediator channels
    install_mediator_channels(username, password, apiUrl, rejectUnauthorized, urn, ['Tutorial Mediator Python'])

    # activate heartbeat thread
    def poll_config():
        while True:
            authenticate(username, apiUrl, rejectUnauthorized)
            new_config = send_heartbeat(username, password, apiUrl, rejectUnauthorized, urn, True)
            logging.info("heartbeat: %s", new_config)
            if (len(new_config) > 0):
                config = new_config
            time.sleep(heartbeat_interval)

    heartbeatThread = threading.Thread(target = poll_config) 
    heartbeatThread.start()

except Exception as e:
    logging.error(e)
    exit()

from api.encounters import *

if __name__ == '__main__':
    # do not run on port 80 on development
    if (port != 80):
        app.run(port=port)
    else:
        app.run()